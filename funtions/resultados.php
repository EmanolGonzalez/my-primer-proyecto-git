<?php
    include_once 'ICM.php';
    include_once 'imgmuestra.php';

    class Resultados{

        public $nombre = "Fulanito";
        public $apellido ="Fulano";
        public $edad = 18;
        public $sexo = "male";
        public $peso = 52;
        public $estatura = 168;
        public $imcEn = array("Bajo Peso"=>"Under weight",
                              "Peso Normal"=>"Normal weight",
                              "Sobre Peso"=>"Overweight",
                              "Obesidad Grado I"=>"Obesity Grade I",
                              "Obesidad Grado II"=>"Obesity Grade II");

        public function traerNombre(){
            if(isset($_POST['name']) && isset($_POST['lastname'])){
                $this->nombre = ucwords(strtolower($_POST['name']));
                $this->apellido = ucwords(strtolower($_POST['lastname']));
                $nombre_completo = $this->nombre ." ". $this->apellido;
                return  $nombre_completo;
            }else{
                return "Usuario Desconocido";
            }
        }
        public function traerEdad()
        {
            if (isset($_POST['edad'])) {
                $this->edad = $_POST['edad'];
                return $this->edad;
            }else
                return $this->edad;
            
        }
        public function traerSexo()
        {
            if (isset($_POST['genero'])) {
                $this->sexo = $_POST['genero'];
                return $this->sexo;
            }else
                return $this->sexo;
            
        }
        public function traerPeso()
        {
            if (isset($_POST['peso'])) {
                $this->peso = $_POST['peso'];
                return $this->peso;
            }else
                return $this->peso;
            
        }
        public function traerEstatura()
        {
            if (isset($_POST['estatura'])) {
                $this->estatura = $_POST['estatura']/100;
                return $this->estatura;
            }else
                return $this->estatura;
            
        }
        public function traerImc()
        {
            if (isset($_POST['peso'])&&isset($_POST['estatura'])&&isset($_POST['genero'])) {
                $usuarioImc = new ICM();
                $imc = $usuarioImc->calcImc($_POST['peso'],$_POST['estatura']);
                $timc = $usuarioImc->tipoImc($_POST['genero'],$imc);
                return $timc;
            }else{
                $usuarioImc = new ICM();
                $imc = $usuarioImc->calcImc($this->peso,$this->estatura);
                $timc =$usuarioImc->tipoImc($this->sexo,$imc);

                return $timc;
            }
        }
        public function trarImg($imc)
        {
            $img = new Imgmuestra();
            return $img->buscarImg($imc);
            
        }
}          
               
/*
    $prueba = new Resultados();
    echo $prueba->traerNombre();
    echo $prueba->traerImc();
    echo $prueba->traerPeso();
    echo $prueba->traerSexo();
    echo $prueba->traerEstatura();
    echo $prueba->traerEdad();
*/

?>