<?php
    class ICM{

        public function calcImc($peso,$altura){
            $alturaM = $altura/100;
            return round($peso/pow($alturaM,2),2); 
        }

        public function tipoImc($sexo,$imc)
        {
            if ($sexo === "hombre" || $sexo === "mujer") {
                if($imc <18.5)
                    return "Bajo Peso";
                elseif($imc<24.9 && $imc > 18.4)
                    return "Peso Normal";                  
                elseif($imc<29.9 && $imc > 25)
                    return "Sobre Peso";
                elseif($imc<34.9 && $imc > 30)
                    return "Obesidad Grado I";
                elseif($imc<39.9 && $imc > 35)
                    return "Obesidad Grado II";
                elseif($imc > 39.9)
                    return "Obesidad Grado III";   
            }
            /*
            if ($edad< 18) {
                if ($sexo === "male" || $sexo === "female") {
                    echo "es menor de edad";
                    if($imc <18.5)
                        echo "Bajo Peso";
                    elseif($imc<24.9 && $imc > 18.4)
                        echo "Peso Normal";                  
                    elseif($imc<29.9 && $imc > 25)
                        echo "Sobre Peso";
                    elseif($imc<34.9 && $imc > 30)
                        echo "Obsidad Grado I";
                    elseif($imc<39.9 && $imc > 35)
                        echo "Obsidad Grado II";
                    elseif($imc > 39.9)
                        echo "Obsidad Grado II";   
                }
            }elseif($edad>=18){
                if ($sexo === "male") {
                    echo "es varon <br>";
                    echo "es mayor de edad <br>"; 
                    if($imc <18.5)
                        echo "Bajo Peso";
                    elseif($imc<24.9 && $imc > 18.4)
                        echo "Peso Normal";                  
                    elseif($imc<29.9 && $imc > 25)
                        echo "Sobre Peso";
                    elseif($imc<34.9 && $imc > 30)
                        echo "Obsidad Grado I";
                    elseif($imc<39.9 && $imc > 35)
                        echo "Obsidad Grado II";
                    elseif($imc > 39.9)
                        echo "Obsidad Grado II";
                }else{
                    echo "es chica <br>";
                    echo "es mayor de edad";
                    if($imc <18.5)
                        echo "Bajo Peso";
                    elseif($imc<24.9 && $imc > 18.4)
                        echo "Peso Normal";                  
                    elseif($imc<29.9 && $imc > 25)
                        echo "Sobre Peso";
                    elseif($imc<34.9 && $imc > 30)
                        echo "Obsidad Grado I";
                    elseif($imc<39.9 && $imc > 35)
                        echo "Obsidad Grado II";
                    elseif($imc > 39.9)
                        echo "Obsidad Grado II";  
                }
            }*/
        }

    }

    /*$prueba = new ICM();
    echo $prueba->tipoImc("male",18.5);*/


?>