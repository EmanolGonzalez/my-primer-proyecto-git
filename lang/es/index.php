<?php
        if (!isset($_COOKIE['count'])) {
            setcookie('count',1, time() + (86400 * 30));
        }else {
            setcookie('count', $_COOKIE['count']+1, time() + (86400 * 30) ); 
        }
           
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PARCIAL-INICIO</title>
    <!--estilos-->
        <link rel="stylesheet" href="../../public/scss/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital,wght@0,100;0,300;0,400;1,100;1,300;1,400&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/4b4403fc43.js" crossorigin="anonymous"></script>
    <!--/estilos-->
</head>
<body>

    <section class="main">
        <div class="main-overlay">
                 <header class="header">
                    <div class="header-logo">
                        <a href="index.php"><i class="fas fa-heartbeat"></i> Healty Hope</a>
                    </div>
                    <nav class="header-links">
                            <a class="header-link active" href="index.php">Inicio</a>              
                            <a class="header-link" href="">Blog</a>
                            <a class="header-link" href="">Productos</a>
                            <div class="header-link lang">
                                <script language="javascript" type="text/javascript">
                                    function sendLanguage(idioma) {
                                        location.href = "../../cookie.php?userLang=" + idioma;
                                    }
                                </script>
                                <div class="lang-container">
                                    <img src="../../public/img/flags/spain.png" alt="">
                                    <span>Es</span>
                                </div>
                                <i class="fas fa-angle-down"></i>
                                <div class="lang-options ">
                                    <a href="javascript:sendLanguage('es');" class="lang-option">
                                        <img src="../../public/img/flags/spain.png" alt="">
                                        <span>Es</span>
                                    </a>
                                    <a href="javascript:sendLanguage('en');" class="lang-option">
                                        <img src="../../public/img/flags/usa.png" alt="">
                                        <span>En</span>
                                    </a>
                                </div>

                            </div>

                    </nav>
                </header>  
                <div class="welcome-wrapper">
                    <h1 class="welcome-title">Bienvenido a Healty Hope</h1>
                    <div class="cards">
                        <div class="card">
                            <i class="fas fa-running"></i>
                            <h2 class="card-title">Da siempre lo mejor de ti</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                        <div class="card">
                            <i class="fas fa-dumbbell"></i>
                            <h2 class="card-title">Esfuerzo hasta lograrlo</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                        <div class="card">
                            <i class="fas fa-heartbeat"></i>
                            <h2 class="card-title">El beneficio es  grande</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                    </div>
                    <a href="#pformulario" class="btn">Comencemos</a>
                </div>
                <div class="visitas">
                    <h2><?php echo($_COOKIE['count']); ?> <span>Visitas</span></h2>
                </div>
        </div>
    </section>

    <section class="contenido">
        <div class="container">
            <div class="articles">
                <div class="article">
                    <div class="article-imgs">
                        <img src="https://images.unsplash.com/photo-1553531889-65d9c41c2609?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80" alt="">
                    </div>
                    <div class="article-information">
                        <h2 class="article-title">Una buena dieta</h2>
                        <p class="article-legend">
                        Una buena dieta consta no de lo que comemos sino de la cantidad de lo que comemos, el equilibrio entre carnes, vegetales y frutas es indispensable para un cuerpo sano y fuerte que pueda afrontar el día a día de la mejor manera.
                        </p>
                    </div>
                    <button class="btn">Descubrelo aqui</button>
                </div>

                <div class="article">
                    <div class="article-imgs">
                        <img src="https://images.unsplash.com/photo-1538805060514-97d9cc17730c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80" alt="">
                    </div>
                    <div class="article-information">
                        <h2 class="article-title">Beneficios de correr</h2>
                        <p class="article-legend">
                        Una buena sesión de cardio puede desembocar en una serie de beneficios que nos brindarán salud y una condición física excelente para pasar tiempo con nuestros seres queridos.
                        </p>
                    </div>
                    <button class="btn">Descubrelo aqui</button>
                </div>
            </div>

        </div>
    </section>

    <section id="pformulario" class="formulario">
        <div class="form-wrapper container">
            <h1>Llena esta madre para saber si estas fit o fat xD</h1>
            <form class="cuestions-form" action="results.php" method="POST">
                <div class="input-wrapper">
                    <input id="name" name="name" type="text" autocomplete="off" required>
                    <label class="label" for="name">Nombre</label>
               </div>
               <div class="input-wrapper">
                    <input id="lastname" name="lastname" type="text" autocomplete="off" required>
                    <label class="label" for="lastname">Apellido</label>
               </div>
               <div class="input-wrapper">
                    <input id="edad" name="edad" type="number" autocomplete="off" required>
                    <label class="label" for="edad">Edad</label>
               </div>
                <div class="radio-wrapper span-row-3">
                    <h2 class="radio-title">SEXO</h2>
                    <div class="form-radios">
                        <div class="fomr-radio">
                            <input type="radio" id="male" name="genero" value="hombre" required/>
                            <label for="male">Hombre</label>
                        </div>
                        <div class="fomr-radio">
                            <input type="radio" id="female" name="genero" value="mujer">
                            <label for="male">Mujer</label>
                        </div>
                    </div>
               </div>
               <div class="input-wrapper">
                <input id="peso" name="peso"  type="number" step="0.001" autocomplete="off" required>
                <label for="peso">Peso kg</label>
                </div>
               <div class="input-wrapper">
                  <input id="estatura" name="estatura" type="number" step="0.001" autocomplete="off" required>
                  <label for="estatura">Estatura cm</label>
               </div>
               <div class="form-submit span-col-2">
                  <input class=" btn btn-wht" id="btn-sbt" name="btn-sbt" type="submit" value="Enviar">
               </div>
            </form>
        </div>
        <footer class="container footer">
            &copy; G-III <span>todos los derechos recervados.</span>
        </footer>
    </section>

    <script src="../../public/js/app.js"></script>
    
</body>
</html>