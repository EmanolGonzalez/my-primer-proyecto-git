<?php
    include_once "../../funtions/resultados.php";

    if (isset($_REQUEST['btn-sbt'])) {
        $res = new Resultados();
        $nombre = $res->traerNombre();
        $edad = $res->traerEdad(); 
        $peso = $res->traerPeso(); 
        $estatura = $res->traerEstatura(); 
        $genero = $res->traerSexo(); 
        $imc = $res->traerImc();  
        $img = $res->trarImg($imc);
    }else{
        $res = new Resultados();
        $nombre = $res->traerNombre();
        $edad = $res->traerEdad(); 
        $peso = $res->traerPeso(); 
        $estatura = $res->traerEstatura(); 
        $genero = $res->traerSexo(); 
        $imc = $res->traerImc();
    }


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PARCIAL-RESULTADO</title>
    <!--estilos-->
        <link rel="stylesheet" href="../../public/scss/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital,wght@0,100;0,300;0,400;1,100;1,300;1,400&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/4b4403fc43.js" crossorigin="anonymous"></script>
    <!--/estilos-->

</head>
<body>

    <section class="result-main">
        <div class="result-main-overlay">
                 <header class="header">
                    <div class="header-logo">
                        <a href="index.php"><i class="fas fa-heartbeat"></i> Healty Hope</a>
                    </div>
                    <nav class="header-links">
                        <a class="header-link active" href="index.php">Inicio</a>              
                            <a class="header-link" href="">Blog</a>
                            <a class="header-link" href="">Productos</a>
                    </nav>
                </header> 
                
                <div class="resultados-wrapper">
                    <div class="resultado-img">
                        <img src="../../public/img/resultados/<?php echo $_POST['genero'];?>/<?php echo $_POST['genero'];?>-<?php echo $img ?>.png" >
                    </div>
                    <div class="resultado-informacion">
                        <div class="resultado-item span-col-2"><h2 class="resultado-nombre "><?php echo $nombre?></h2></div>
                        <div class="resultado-item"><p><span>Edad: </span><?php echo" "; echo $edad?> años</p></div>
                        <div class="resultado-item"><p><span>Sexo: </span><?php echo" "; echo $genero?></p></div>
                        <div class="resultado-item"><p> <span>Peso: </span><?php echo" "; echo $peso?> Kg</p> </div>
                        <div class="resultado-item"><p> <span>Estatura: </span><?php echo" "; echo $estatura?> metros</p></div>
                        <div class="resultado-item span-col-2"> <span class="resultado-imc"> <?php echo" ";echo $imc?></span></div>
                    </div>
                </div>

                <footer class="container footer">
                    &copy; G-III <span>todos los derechos recervados.</span>
                </footer>
        </div>
    </section>

    <script src="../../public/js/app.js"></script>
    
</body>
</html>