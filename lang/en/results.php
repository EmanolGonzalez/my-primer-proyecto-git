<?php
    include_once "../../funtions/resultados.php";

    if (isset($_REQUEST['btn-sbt'])) {
        $res = new Resultados();
        $nombre = $res->traerNombre();
        $edad = $res->traerEdad(); 
        $peso = $res->traerPeso(); 
        $estatura = $res->traerEstatura(); 
        $genero = $res->traerSexo(); 
        $imc = $res->traerImc();
        $imcEn = $res->imcEn[$imc];  
        $img = $res->trarImg($imc);

    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PARCIAL-RESULTADO</title>
    <!--estilos-->
        <link rel="stylesheet" href="../../public/scss/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital,wght@0,100;0,300;0,400;1,100;1,300;1,400&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/4b4403fc43.js" crossorigin="anonymous"></script>
    <!--/estilos-->

</head>
<body>

    <section class="result-main">
        <div class="result-main-overlay">
            <header class="header">
                <div class="header-logo">
                    <a href="index.php"><i class="fas fa-heartbeat"></i> Healty Hope</a>
                </div>
                <nav class="header-links">
                        <a class="header-link active" href="index.php">Home</a>              
                        <a class="header-link" href="">Blog</a>
                        <a class="header-link" href="">Products</a>
                </nav>
            </header> 
            
            <div class="resultados-wrapper">
                <div class="resultado-img">
                    <img src="../../public/img/resultados/<?php echo $_POST['genero'];?>/<?php echo $_POST['genero'];?>-<?php echo $img ?>.png" >
                </div>
                <div class="resultado-informacion">
                <div class="resultado-item span-col-2"><h2 class="resultado-nombre "><?php echo $nombre?></h2></div>
                    <div class="resultado-item"><p><span>Years: </span><?php  echo $edad?> olds</p></div>
                    <div class="resultado-item"><p><span>Gender: </span><?php  echo ($genero == "hombre")?"Male":"Female";?></p></div>
                    <div class="resultado-item"><p> <span>Weight: </span><?php  echo $peso?> Kg</p> </div>
                    <div class="resultado-item"><p> <span>Stature: </span><?php  echo $estatura?> metre</p></div>
                    <div class="resultado-item span-col-2"> <span class="resultado-imc"> <?php echo $imcEn;?></span></div>
                </div>

            </div>

            <footer class="container footer">
            &copy; G-III <span>all rights recerved.</span>
            </footer>
        </div>
    </section>

    <script src="../../public/js/app.js"></script>
    
</body>
</html>