<?php
        if (!isset($_COOKIE['count'])) {
            setcookie('count',1, time() + (86400 * 30));
        }else {
            setcookie('count', $_COOKIE['count']+1, time() + (86400 * 30) ); 
        }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PARCIAL-INICIO</title>
    <!--estilos-->
        <link rel="stylesheet" href="../../public/scss/style.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lato&family=Roboto:ital,wght@0,100;0,300;0,400;1,100;1,300;1,400&display=swap" rel="stylesheet">
        <script src="https://kit.fontawesome.com/4b4403fc43.js" crossorigin="anonymous"></script>
    <!--/estilos-->

</head>
<body>

    <section class="main">
        <div class="main-overlay">
                 <header class="header">
                    <div class="header-logo">
                        <a href="index.php"><i class="fas fa-heartbeat"></i> Healty Hope</a>
                    </div>
                    <nav class="header-links">
                    <a class="header-link active" href="index.php">Home</a>              
                            <a class="header-link" href="">Blog</a>
                            <a class="header-link" href="">Products</a>
                            <div class="header-link lang">
                                <script language="javascript" type="text/javascript">
                                    function sendLanguage(idioma) {
                                        location.href = "../../cookie.php?userLang=" + idioma;
                                    }
                                </script>
                                <div class="lang-container">
                                    <img src="../../public/img/flags/usa.png" alt="">
                                    <span>En</span>
                                </div>
                                <i class="fas fa-angle-down"></i>
                                <div class="lang-options ">
                                    <a href="javascript:sendLanguage('es');" class="lang-option">
                                        <img src="../../public/img/flags/spain.png" alt="">
                                        <span>Es</span>
                                    </a>
                                    <a href="javascript:sendLanguage('en');" class="lang-option">
                                        <img src="../../public/img/flags/usa.png" alt="">
                                        <span>En</span>
                                    </a>
                                </div>

                            </div>

                    </nav>
                </header>  
                <div class="welcome-wrapper">
                    <h1 class="welcome-title">Welcome To Healty Hope</h1>
                    <div class="cards">
                        <div class="card">
                            <i class="fas fa-running"></i>
                            <h2 class="card-title">Always do your best</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                        <div class="card">
                            <i class="fas fa-dumbbell"></i>
                            <h2 class="card-title">Effort to achieve this</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                        <div class="card">
                            <i class="fas fa-heartbeat"></i>
                            <h2 class="card-title">The profit is great</h2>
                            <p class="card-legend">Lo que siembres hoy dará su fruto mañana</p>
                        </div>
                    </div>
                    <a href="#pformulario" class="btn">Started</a>
                </div>
                <div class="visitas">
                <h2><?php echo($_COOKIE['count']); ?> <span>Visits</span></h2>
                </div>
        </div>
    </section>

    <section class="contenido">
        <div class="container">
            <div class="articles">
                <div class="article">
                    <div class="article-imgs">
                        <img src="https://images.unsplash.com/photo-1553531889-65d9c41c2609?ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1500&q=80" alt="">
                    </div>
                    <div class="article-information">
                        <h2 class="article-title">A good diet</h2>
                        <p class="article-legend">
                        A good diet consists not of what we eat but of the amount of what we eat, the balance between meats, vegetables and fruits is essential for a healthy and strong body that can face the day to day in the best way.
                        </p>
                    </div>
                    <button class="btn">Let's go</button>
                </div>

                <div class="article">
                    <div class="article-imgs">
                        <img src="https://images.unsplash.com/photo-1538805060514-97d9cc17730c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80" alt="">
                    </div>
                    <div class="article-information">
                        <h2 class="article-title">Benefits of running</h2>
                        <p class="article-legend">
                        A good cardio session can lead to a series of benefits that will give us health and excellent physical condition to spend time with our loved ones
                        </p>
                    </div>
                    <button class="btn">Let's go</button>
                </div>
            </div>

        </div>
    </section>

    <section id="pformulario" class="formulario">
    <div class="form-wrapper container">
            <h1>Fill this thing to see if you're fit or fat xD</h1>
            <form class="cuestions-form" action="results.php" method="POST">
                <div class="input-wrapper">
                    <input id="name" name="name" type="text" autocomplete="off" required>
                    <label class="label" for="name">Name</label>
               </div>
               <div class="input-wrapper">
                    <input id="lastname" name="lastname" type="text" autocomplete="off" required>
                    <label class="label" for="lastname">Lastname</label>
               </div>
               <div class="input-wrapper">
                    <input id="edad" name="edad" type="number" autocomplete="off" required>
                    <label class="label" for="edad">Years</label>
               </div>
                <div class="radio-wrapper span-row-3">
                    <h2 class="radio-title">Gender</h2>
                    <div class="form-radios">
                        <div class="fomr-radio">
                            <input type="radio" id="male" name="genero" value="hombre" required/>
                            <label for="male">Male</label>
                        </div>
                        <div class="fomr-radio">
                            <input type="radio" id="female" name="genero" value="mujer">
                            <label for="male">Female</label>
                        </div>
                    </div>
               </div>
               <div class="input-wrapper">
                <input id="peso" name="peso"  type="number" step="0.001" autocomplete="off" required>
                <label for="peso">Weight kg</label>
                </div>
               <div class="input-wrapper">
                  <input id="estatura" name="estatura" type="number" step="0.001" autocomplete="off" required>
                  <label for="estatura">Stature cm</label>
               </div>
               <div class="form-submit span-col-2">
                  <input class=" btn btn-wht" id="btn-sbt" name="btn-sbt" type="submit" value="Enviar">
               </div>
            </form>
        </div>
        <footer class="container footer">
            &copy; G-III <span>all rights recerved.</span>
        </footer>
    </section>   

    <script src="../../public/js/app.js"></script>
    
</body>
</html>